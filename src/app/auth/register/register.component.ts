import { Component, OnInit, ChangeDetectorRef, Inject } from '@angular/core';
import { NbAuthSocialLink, NbAuthService, NB_AUTH_OPTIONS, NbAuthResult } from '@nebular/auth';
import { Router } from '@angular/router';
import { getDeepFromObject } from 'src/app/_helpers/auth-helpers';
import { NbDialogService } from '@nebular/theme';
import { VerificationDialogComponent } from 'src/app/_components/dialog/verification-code/verification-dialog.component';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {


  redirectDelay: number = 0;
  showMessages: any = {};
  strategy: string = '';

  submitted = false;
  errors: string[] = [];
  messages: string[] = [];
  user: any = {};
  socialLinks: NbAuthSocialLink[] = [];
  verification_code: any = null;

  constructor(protected service: NbAuthService,
    private dialogService: NbDialogService,
    @Inject(NB_AUTH_OPTIONS) protected options = {},
    protected cd: ChangeDetectorRef,
    protected router: Router) {

    this.redirectDelay = this.getConfigValue('forms.register.redirectDelay');
    this.showMessages = this.getConfigValue('forms.register.showMessages');
    this.strategy = this.getConfigValue('forms.register.strategy');
    this.socialLinks = this.getConfigValue('forms.login.socialLinks');
  }

  register(): void {
    this.errors = this.messages = [];
    this.submitted = true;
    this.user.verification_code = this.verification_code != null ? this.verification_code : null;
    this.service.register(this.strategy, this.user).subscribe((result) => {
      this.submitted = false;

      if (result.isSuccess()) {
        this.messages = result.getMessages();
      } else {

        this.errors = result.getErrors();
        this.errors = result.getResponse();

        if (this.errors[0] == 'Two Factor Required') {
          this.open();
        }
      }
      this.user.verification_code = null;
      const redirect = result.getRedirect();
      if (redirect) {
        setTimeout(() => {
          return this.router.navigateByUrl(redirect);
        }, this.redirectDelay);
      }
      this.cd.detectChanges();
    });
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.options, key, null);
  }

  open() {
    this.dialogService.open(VerificationDialogComponent)
      .onClose.subscribe(code => {
        this.verification_code = code;
        this.register()
      });
  }
}
