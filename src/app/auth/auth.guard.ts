import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanLoad, CanActivateChild, Route } from '@angular/router';
import { AuthService } from './auth.service';
import { NbAuthService } from '@nebular/auth';
import { tap } from 'rxjs/operators';


@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate, CanLoad, CanActivateChild {
  constructor(private authService: NbAuthService,
    private router: Router,
    private authenticationService: AuthService
  ) { }

  canActivate() {
    return this.authService.isAuthenticated()
      .pipe(
        tap(authenticated => {
          if (!authenticated) {
            this.router.navigate(['auth/login']);
          }
        }),
      );
  } // canActive can return Observable<boolean>, which is exactly what isAuthenticated returns



  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.canActivate();
  }

  canLoad(route: Route): boolean {
    let url = '/${route.path}';

    return this.checkLogin(url);
  }

  checkLogin(url: string): boolean {
    if (this.authenticationService.currentUserValue) { return true; }
    // Store the attempted URL for redirecting
    this.authenticationService.redirectUrl = url;
    this.router.navigate(['/auth/login']);
    return false;
  }

}