import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { AuthRoutingModule } from './auth-routing.module';
import { AlertComponent } from '../_components/alert/alert.component';
import { NbAuthModule } from '@nebular/auth';
import { NbCheckboxModule, NbAlertModule, NbIconModule } from '@nebular/theme';
import { RouterModule } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { SharedModule } from '../shared.module';

@NgModule({
  imports: [
    AuthRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
    NbAlertModule,
    NbCheckboxModule,
    NbIconModule,
    NbAuthModule,
    SharedModule
  ],
  declarations: [
    LoginComponent,
    AlertComponent,
    RegisterComponent,
  ]
})
export class AuthModule { }