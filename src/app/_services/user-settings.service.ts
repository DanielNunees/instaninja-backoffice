import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ReferenceProfile } from '../_models/reference-profile';
import { map } from 'rxjs/operators';
import { UserSettigns } from '../_models/user-settigns';

@Injectable({
  providedIn: 'root'
})
export class UserSettingsService {

  constructor(private http: HttpClient) { }

  getAllReferenceProfiles() {
    return this.http.get<ReferenceProfile[]>(environment.apiUrl + `/user/allReferences`)
      .pipe(map(user_stats => {
        return user_stats;
      }));
  }

  getUserSettings() {
    return this.http.get<UserSettigns>(environment.apiUrl + `/user/settings`)
      .pipe(map(user_stats => {
        return user_stats;
      }));
  }

  registerReferenceProfiles(referenceProfiles) {
    return this.http.post<ReferenceProfile[]>(environment.apiUrl + `/user/reference/profile/new`, { 'reference_profile': referenceProfiles })
      .pipe(map(response => {
        return response;
      }));
  };
  registerReferenceHashtag(referenceHashtag) {
    return this.http.post(environment.apiUrl + `/user/reference/hahstag/new`, { 'reference_hashtag': referenceHashtag })
      .pipe(map(response => {
        return response;
      }));
  };
  registerReferenceLocation(referenceLocation) {
    return this.http.post(environment.apiUrl + `/user/reference/location/new`, { 'reference_location': referenceLocation })
      .pipe(map(response => {
        return response;
      }));
  };

  registerUserSettings(userSettigns: UserSettigns) {
    return this.http.post<UserSettigns[]>(environment.apiUrl + `/user/settings`, userSettigns)
      .pipe(map(response => {
        return response;
      }));
  }

  findPlaces(location) {
    return this.http.get(environment.apiUrl + `/user/reference/location/find/` + location)
      .pipe(map(response => {
        return response;
      }));
  }
}
