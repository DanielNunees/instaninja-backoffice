import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from "../../environments/environment";
import { UserStats } from '../_models/user_stats';
import { UserLog } from '../_models/user-logs';

@Injectable({
  providedIn: 'root'
})
export class UserStatsService {

  constructor(private http: HttpClient) { }

  getAll(quantity: number) {
    return this.http.get<UserStats[]>(environment.apiUrl + `/user/stats/` + quantity)
      .pipe(map(user_stats => {
        return user_stats;
      }));
  }

  checkStatus() {
    return this.http.get(environment.apiUrl + `/checkStatus`)
      .pipe(map(user_stats => {
        return user_stats;
      }));
  }
}



