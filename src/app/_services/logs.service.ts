import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserLog } from '../_models/user-logs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { ExceptiongLog } from '../_models/exception-logs';
import { SystemLog } from '../_models/system-logs';

@Injectable({
  providedIn: 'root'
})
export class LogsService {

  constructor(private http: HttpClient) { }


  getUserLogs(quantity?: number) {
    return this.http.get<UserLog[]>(environment.apiUrl + `/logs/user/`+100)
      .pipe(map(user_logs => {
        return user_logs;
      }));
  }
  getSystemLogs(quantity?: number) {
    return this.http.get<SystemLog[]>(environment.apiUrl + `/logs/system/`+100)
      .pipe(map(system_logs => {
        return system_logs;
      }));
  }
  getExceptionLogs(quantity?: number) {
    return this.http.get<ExceptiongLog[]>(environment.apiUrl + `/logs/exception/`+100)
      .pipe(map(exception_logs => {
        return exception_logs;
      }));
  }
}
