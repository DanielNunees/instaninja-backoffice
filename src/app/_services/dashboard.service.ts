import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient) { }


  startfollowFollowers() {
    return this.http.get(environment.apiUrl + `/action/follow/start`)
      .pipe(map(response => {
        return response;
      }));
  }

  stopFollowFollowers() {
    return this.http.get(environment.apiUrl + `/action/follow/stop`)
      .pipe(map(response => {
        return response;
      }));
  }

  startLikes() {
    return this.http.get(environment.apiUrl + `/action/likes/start`)
      .pipe(map(response => {
        return response;
      }));
  }
  stopLikes() {
    return this.http.get(environment.apiUrl + `/action/likes/stop`)
      .pipe(map(response => {
        return response;
      }));
  }
}
