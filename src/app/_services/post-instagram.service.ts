import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { InstagramPost } from '../_models/instagram-post';

@Injectable({
  providedIn: 'root'
})
export class PostInstagramService {

  constructor(private http: HttpClient) { }

  getAllOptions() {
    return this.http.get(environment.apiUrl + `/posts/options`)
      .pipe(map(response => {
        return response;
      }));
  };


  getAllPosts() {
    return this.http.get(environment.apiUrl + `/posts/timeline`)
      .pipe(map(response => {
        return response;
      }));
  };

  getAllStories() {
    return this.http.get(environment.apiUrl + `/posts/story`)
      .pipe(map(response => {
        return response;
      }));
  };

  getPost(id) {
    return this.http.get(environment.apiUrl + `/posts/timeline/` + id)
      .pipe(map(response => {
        return response;
      }));
  };

  createNewPost(instagramPost) {
    return this.http.post<InstagramPost[]>(environment.apiUrl + `/posts/timeline/new`, instagramPost)
      .pipe(map(response => {
        return response;
      }));
  }

  createNewStory(instagramStory) {
    return this.http.post<InstagramPost[]>(environment.apiUrl + `/posts/story/new`, instagramStory)
      .pipe(map(response => {
        return response;
      }));
  }

  getAllComments() {
    return this.http.get(environment.apiUrl + `/posts/comments`)
      .pipe(map(response => {
        return response;
      }));
  };

  createNewComment(comment) {
    return this.http.post(environment.apiUrl + `/posts/comment/new`, comment)
      .pipe(map(response => {
        return response;
      }));
  }

}
