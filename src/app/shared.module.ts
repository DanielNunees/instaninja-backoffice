import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DialogComponent } from './_components/dialog/new-reference/dialog.component';
import { NbCardModule, NbButtonModule, NbInputModule, NbIconModule } from '@nebular/theme';
import { VerificationDialogComponent } from './_components/dialog/verification-code/verification-dialog.component';
import { FindLocationComponent } from './_components/dialog/find-location/find-location.component';
import { ShortNumbersPipe } from './_pipes/short-numbers.pipe';

@NgModule({
    imports: [
        CommonModule,
        NbCardModule,
        NbButtonModule,
        NbIconModule,
        NbInputModule,
        FormsModule],
    declarations: [
        DialogComponent,
        VerificationDialogComponent,
        FindLocationComponent,
        ShortNumbersPipe,
    ],
    exports: [
        CommonModule,
        DialogComponent,
        VerificationDialogComponent,
        FindLocationComponent,
        NbCardModule,
        FormsModule,
        NbButtonModule,
        NbIconModule,
        ShortNumbersPipe,
        NbInputModule,
    ],
    entryComponents: [DialogComponent, FindLocationComponent, VerificationDialogComponent]
})
export class SharedModule { }