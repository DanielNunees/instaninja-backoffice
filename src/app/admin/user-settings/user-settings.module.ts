import { NgModule } from '@angular/core';
import { NbAccordionModule, NbListModule, NbTabsetModule } from '@nebular/theme';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { PeopleComponent } from './people/people.component';
import { ReferencesComponent } from './references/references.component';
import { UserSettingRoutingModule } from './user-settings-routing.module';
import { Ng5SliderModule } from 'ng5-slider';
import { ReferencesModule } from './references/references.module';
import { SharedModule } from 'src/app/shared.module';


@NgModule({
    declarations: [
        PeopleComponent,
        ReferencesComponent,
    ],
    imports: [
        NgbDatepickerModule,
        ReferencesModule,
        NbTabsetModule,
        Ng5SliderModule,
        NbListModule,
        NbAccordionModule,
        ReactiveFormsModule,
        UserSettingRoutingModule,
        SharedModule
    ]
})
export class UserSettingsModule { }
