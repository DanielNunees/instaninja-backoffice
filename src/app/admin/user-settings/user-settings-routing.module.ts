import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/auth/auth.guard';
import { ReferencesComponent } from './references/references.component';
import { PeopleComponent } from './people/people.component';
const instagramRoutes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        children: [

            {
                path: 'references',
                component: ReferencesComponent,
                canActivate: [AuthGuard],
            },
            {
                path: 'people',
                component: PeopleComponent,
                canActivate: [AuthGuard],
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(instagramRoutes)],
    exports: [RouterModule],
    providers: []
})
export class UserSettingRoutingModule { }
