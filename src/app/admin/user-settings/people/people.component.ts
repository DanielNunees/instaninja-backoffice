import { Component, OnInit } from '@angular/core';
import { UserSettingsService } from 'src/app/_services/user-settings.service';
import { FormBuilder } from '@angular/forms';
import { UserSettigns } from 'src/app/_models/user-settigns';
import { Options, LabelType } from 'ng5-slider';
import { Subscription } from "rxjs/Subscription";
import { ActivatedRoute } from '@angular/router';
import { NbToastrService } from '@nebular/theme';


@Component({
  selector: 'people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.scss']
})
export class PeopleComponent implements OnInit {

  private subscription: Subscription;
  referenceProfiles: any;
  referenceHashtag: any;
  referenceLocations: any;
  private index: number = 0;
  userSettings: UserSettigns;
  //Table Parameters
  page = 1;
  pageSize = 10;
  collectionSize = 10;
  loaded = false;
  constructor(private userSettingsService: UserSettingsService) { }

  //Slider Range Time Parameters
  time_options: Options = {
    floor: 0,
    ceil: 150,
    step: 1,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return '<b>Min: </b>' + value + ' Min';
        case LabelType.High:
          return '<b>Max: </b>' + value + ' Min';
        default:
          return '' + value + ' Min';
      }
    }
  };

  //Slider Range Action Parameters
  action_options: Options = {
    floor: 0,
    ceil: 30,
    step: 1,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return '<b>Min: </b>' + value;
        case LabelType.High:
          return '<b>Max: </b>' + value;
        default:
          return '' + value;
      }
    }
  };

  //Slider Range Action Parameters
  following_max_option: Options = {
    floor: 100,
    ceil: 1000,
    step: 1,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return '<b>Min: </b>' + value;
        case LabelType.High:
          return '<b>Max: </b>' + value;
        default:
          return '' + value;
      }
    }
  };

  ngOnInit() {

    this.getUserSettigns();

  }



  getUserSettigns() {
    const subscription = this.subscription = this.userSettingsService.getUserSettings().subscribe(data => {
      this.userSettings = data[0];
      this.loaded = true;
    });
    this.subscription.add(subscription);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }




  onSubmit() {
    const subscription = this.userSettingsService.registerUserSettings(this.userSettings).subscribe(data => {

    });
    //this.subscription.add(subscription);
  }

}
