import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserSettingsService } from 'src/app/_services/user-settings.service';
import { NbDialogService } from '@nebular/theme';
import { DialogComponent } from 'src/app/_components/dialog/new-reference/dialog.component';

@Component({
  selector: 'references',
  templateUrl: './references.component.html',
  styleUrls: ['./references.component.scss'],
  styles: [`
    :host nb-tabset {
      padding: 0px;
    }
  `]
})

export class ReferencesComponent implements OnInit, OnDestroy {
  referenceProfiles: any;
  referenceHashtag: any;
  referenceLocations: any;
  subscription: Subscription = new Subscription();
  names: string[] = [];

  constructor(private userSettingsService: UserSettingsService,
    private dialogService: NbDialogService) { }

  ngOnInit() {
    this.getAllReferecenProfiles();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getAllReferecenProfiles() {
    const sub = this.userSettingsService.getAllReferenceProfiles().subscribe(data => {
      this.referenceProfiles = data['reference_profile'];
      this.referenceHashtag = data['reference_hashtag'];
      this.referenceLocations = data['reference_location'];
    });

    this.subscription.add(sub);

  }

  open(value: any) {
    this.dialogService.open(DialogComponent)
      .onClose.subscribe(name => { value = name, this.names.push(name), this.onSubmit(name) });
  }

  onSubmit(name: any) {
    this.userSettingsService.registerReferenceProfiles(name).subscribe(data => {
    })
  }

}
