import { Component, OnInit, Input } from '@angular/core';
import { UserSettingsService } from 'src/app/_services/user-settings.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'reference-location',
  templateUrl: './reference-location.component.html',
  styleUrls: ['./reference-location.component.scss']
})
export class ReferenceLocationComponent implements OnInit {
  @Input() referenceLocations: any;
  subscription: Subscription = new Subscription();
  places: any;
  constructor(private userSettingsService: UserSettingsService) { }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscription.unsubscribe();
  }



  search(value) {
    const subs = this.userSettingsService.findPlaces(value).subscribe(data => {
      this.places = data;
    });
    this.subscription.add(subs);
  }

  select(place) {
    const subs = this.userSettingsService.registerReferenceLocation(place.location).subscribe(data => {
      
    });
    this.subscription.add(subs);
  }
}
