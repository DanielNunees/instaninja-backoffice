import { Component, OnInit, Input } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { UserSettingsService } from 'src/app/_services/user-settings.service';
import { DialogComponent } from 'src/app/_components/dialog/new-reference/dialog.component';
import { Subscription } from 'rxjs';


@Component({
  selector: 'reference-profile',
  templateUrl: './reference-profile.component.html',
  styleUrls: ['./reference-profile.component.scss']
})
export class ReferenceProfileComponent implements OnInit {
  @Input() referenceProfiles: any;
  subscription: Subscription = new Subscription();

  constructor(private userSettingsService: UserSettingsService,
    private dialogService: NbDialogService) { }

  ngOnInit() {
  }
  open() {
    if (this.referenceProfiles.length == 3) {
      return;
    }
    this.dialogService.open(DialogComponent)
      .onClose.subscribe(name => { this.onSubmit(name) });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  onSubmit(name) {
    if (name !== '' && name !== undefined) {
      const sub = this.userSettingsService.registerReferenceProfiles(name).subscribe(data => {
      });
      this.subscription.add(sub);
    }
  }
}
