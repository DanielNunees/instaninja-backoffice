import { Component, OnInit, Input } from '@angular/core';
import { DialogComponent } from 'src/app/_components/dialog/new-reference/dialog.component';
import { Subscription } from 'rxjs';
import { NbDialogService } from '@nebular/theme';
import { UserSettingsService } from 'src/app/_services/user-settings.service';

@Component({
  selector: 'reference-hashtag',
  templateUrl: './reference-hashtag.component.html',
  styleUrls: ['./reference-hashtag.component.scss']
})
export class ReferenceHashtagComponent implements OnInit {
  @Input() referenceHashtag: any;
  subscription: Subscription = new Subscription();

  constructor(private userSettingsService: UserSettingsService,
    private dialogService: NbDialogService) { }

  ngOnInit() {
  }
  open() {
    this.dialogService.open(DialogComponent)
      .onClose.subscribe(name => { this.onSubmit(name) });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  onSubmit(name) {
    if (name !== '' && name !== undefined) {
      const sub = this.userSettingsService.registerReferenceHashtag(name).subscribe(data => {
      });
      this.subscription.add(sub);
    }
  }
}
