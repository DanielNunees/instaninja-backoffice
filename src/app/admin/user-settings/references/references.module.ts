import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReferenceLocationComponent } from './reference-location/reference-location.component';
import { ReferenceHashtagComponent } from './reference-hashtag/reference-hashtag.component';
import { ReferenceProfileComponent } from './reference-profile/reference-profile.component';
import { NbButtonModule, NbInputModule } from '@nebular/theme';

@NgModule({
    declarations: [
        ReferenceLocationComponent,
        ReferenceHashtagComponent,
        ReferenceProfileComponent

    ],
    imports: [
        CommonModule,
        NbButtonModule,
        NbInputModule
    ],
    exports: [
        ReferenceLocationComponent,
        ReferenceHashtagComponent,
        ReferenceProfileComponent
    ]
})
export class ReferencesModule { }
