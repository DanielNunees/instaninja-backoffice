import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavBarComponent } from '../_components/nav-bar/nav-bar.component';
import { SideMenuComponent } from '../_components/side-menu/side-menu.component';
import { NgbCollapseModule, NgbAccordionModule, NgbPaginationModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';
import { NbLayoutModule, NbMenuModule, NbSidebarModule, NbCardModule, NbSpinnerModule, NbActionsModule, NbButtonModule, NbIconModule, NbUserModule, NbAccordionModule, NbDialogModule } from '@nebular/theme';
import { NgxSpinnerModule } from 'ngx-spinner';
import { InstagramModule } from './instagram/instagram.module';
import { UserSettingsModule } from './user-settings/user-settings.module';
import { DashboardModule } from './dashboard/dashboard.module';





@NgModule({
  declarations: [
    AdminComponent,
    NavBarComponent,
    SideMenuComponent,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbCollapseModule,
    NbMenuModule,
    NbSidebarModule,
    NbAccordionModule,
    NgbAccordionModule,
    NbDialogModule.forChild(),
    InstagramModule,
    NbButtonModule,
    NbUserModule,
    NbIconModule,
    NbSpinnerModule,
    NbActionsModule,
    NbCardModule,
    NgbAlertModule,
    NgxSpinnerModule,
    NbLayoutModule,
    AngularDateTimePickerModule,
    NgbPaginationModule,
    UserSettingsModule,
    DashboardModule
  ]
})
export class AdminModule { }
