import { Component, OnInit } from '@angular/core';
import { UserStatsService } from 'src/app/_services/user-stats.service';
import { DashboardService } from 'src/app/_services/dashboard.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.scss']
})
export class ActionsComponent implements OnInit {
  follow_status = {
    value: true,
    icon: "",
    badgeText: "",
    badgeStatus: "",
  };
  like_status = {
    value: true,
    icon: "",
    badgeText: "",
    badgeStatus: "",
  };

  private subscription: Subscription = new Subscription();

  constructor(private userStatsService: UserStatsService,
    private dashboardService: DashboardService) { }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  ngOnInit() {
    this.checkStatus();
  }

  likes() {
    if (!this.like_status.value) {
      this.startLikes();
    } else {
      this.stopLikes();
    }
  }

  follow() {
    if (!this.follow_status.value) {
      this.startFollow();
    } else {
      this.stopFollows();
    }
  }

  startLikes() {
    this.subscription.add(this.dashboardService.startLikes().subscribe(data => {
      this.checkStatus();

    }));
  }

  stopLikes() {
    this.subscription.add(this.dashboardService.stopLikes().subscribe(data => {
      this.checkStatus();
    }));
  }

  stopFollows() {
    this.subscription.add(this.dashboardService.stopFollowFollowers().subscribe(data => {
      this.checkStatus();
    }));
  }
  startFollow() {
    this.subscription.add(this.dashboardService.startfollowFollowers().subscribe(data => {
      this.checkStatus();
    }));
  }

  checkStatus() {
    this.subscription.add(this.userStatsService.checkStatus().subscribe(data => {

      this.follow_status = {
        value: data['follow'],
        icon: data['follow'] ? "person" : "person-outline",
        badgeText: data['follow'] ? "ON" : "OFF",
        badgeStatus: data['follow'] ? "info" : "danger",
      };

      this.like_status = {
        value: data['likes'],
        icon: data['likes'] ? "heart" : "heart-outline",
        badgeText: data['likes'] ? "ON" : "OFF",
        badgeStatus: data['likes'] ? "info" : "danger",
      };

    }));

  }

}
