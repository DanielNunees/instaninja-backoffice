import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard.component';
import { SummaryComponent } from './summary/summary.component';
import { ActionsComponent } from './actions/actions.component';
import { SharedModule } from 'src/app/shared.module';
import { NbActionsModule } from '@nebular/theme';
import { DashboardRoutingModule } from './dashboard.routing.module';


@NgModule({
    declarations: [
        DashboardComponent,
        SummaryComponent,
        ActionsComponent
    ],
    imports: [
        SharedModule,
        NbActionsModule,
        DashboardRoutingModule
    ],
})
export class DashboardModule { }
