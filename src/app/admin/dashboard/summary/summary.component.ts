import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/_services/dashboard.service';
import { UserStatsService } from 'src/app/_services/user-stats.service';
import { Subscription } from 'rxjs';
import { UserStats } from 'src/app/_models/user_stats';

@Component({
  selector: 'summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {

  loading = true;


  stats: UserStats;
  private subscription: Subscription = new Subscription();
  constructor(private userStatsService: UserStatsService) { }


  ngOnInit() {
    this.getUserStats();

  }


  getUserStats() {
    this.subscription.add(this.userStatsService.getAll(1).subscribe(data => {
      this.stats = data;
      this.loading = false;
    }));
  }

}
