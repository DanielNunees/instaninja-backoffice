import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PostInstagramService } from 'src/app/_services/post-instagram.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { FindLocationComponent } from 'src/app/_components/dialog/find-location/find-location.component';

@Component({
  selector: 'app-new-story-form',
  templateUrl: './new-story-form.component.html',
  styleUrls: ['./new-story-form.component.scss']
})
export class NewStoryFormComponent implements OnInit {
  instagramStoryForm: FormGroup;
  submitted: boolean;
  imagePath: any;
  options: any;
  imgURL: any;
  start_date: Date = new Date();
  start_date_string: string = new Date().toDateString();
  post_id: string;
  location: any
  caption: string;
  subscription: Subscription = new Subscription();
  public message: string;
  constructor(private fb: FormBuilder,
    private postInstagramService: PostInstagramService,
    private route: ActivatedRoute,
    private router: Router, private dialogService: NbDialogService) { }


  /**
   * It's just possible, for now, to tag 3 friends at same time
   * Just one location
   * One image
   * Obs. Album post coming soon.
   */

  ngOnInit() {
    this.instagramStoryForm = this.fb.group({
      post_location: ['', { disabled: true }],
      tagged_people_list: [''],
      tagged_hashtag_list: [''],
      recurrent_post_frequency: [''],
      hashtag_fav: [''],
      tagged_people1: [''],
      tagged_people2: [''],
      tagged_people3: [''],
      tagged_hashtag1: [''],
      tagged_hashtag2: [''],
      tagged_hashtag3: [''],
      post_at: ['', Validators.required],
      img: ['', Validators.required],

    });
    this.getAllOptions();
    this.getPost();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  open() {
    const hasScroll = true;
    this.dialogService.open(FindLocationComponent, { hasScroll })
      .onClose.subscribe(location => { this.location = location });
  }


  /**
   * Get one post from the database to edit, and show the data in the same form(New post form)
   * 
   */
  getPost() {
    this.post_id = this.route.snapshot.paramMap.get('id');
    if (this.post_id) {
      const subs = this.postInstagramService.getPost(this.post_id).subscribe(data => {
        this.instagramStoryForm.patchValue(data[0]);
        this.start_date = new Date(data[0].post_at);
        this.start_date_string = this.start_date.toLocaleString();

        this.imgURL = data[0].post_media[0].url;
        this.instagramStoryForm.patchValue({ img: '' });

        //Get tagged people
        var i = 0;
        let aux: Array<any> = [];
        data[0].post_user_tags.forEach(element => {
          aux[i] = element.username;
          i++;
        });
        this.instagramStoryForm.patchValue({ tagged_people1: aux[0] });
        this.instagramStoryForm.patchValue({ tagged_people2: aux[1] });
        this.instagramStoryForm.patchValue({ tagged_people3: aux[2] });

      });
      this.subscription.add(subs);
    }

  }


  getAllOptions() {
    const subs = this.postInstagramService.getAllOptions().subscribe(data => {
      this.options = data;
    });
    this.subscription.add(subs);
  }

  onSubmit() {
    this.submitted = true;
    if (!this.instagramStoryForm.valid)
      return;

    let file: File = this.imagePath[0];
    let formData: FormData = new FormData();

    let post_location = null;
    if (this.location) {
      post_location = JSON.stringify(this.location.location);
    }

    let tzoffset = (new Date(this.instagramStoryForm.value.post_at)).getTimezoneOffset() * 60000; //offset in milliseconds
    let post_at = (new Date(this.instagramStoryForm.value.post_at - tzoffset)).toISOString().slice(0, -1);


    formData.append('id', this.post_id);
    formData.append('photo', file, file.name);
    formData.append('post_location', post_location);
    formData.append('recurrent_post_frequency', this.instagramStoryForm.value.recurrent_post_frequency);
    formData.append('tagged_people', this.taggedPeople());
    formData.append('tagged_hashtag', this.taggedHashtag());
    formData.append('caption', this.caption);
    formData.append('post_at', post_at.toString());

    this.instagramStoryForm.value.aux = formData;

    this.postInstagramService.createNewStory(formData).subscribe(data => {
      //this.router.navigate(['/admin/instagram/posts']);
    });
  }


  taggedPeople() {
    const tagged_people_list1 = [this.instagramStoryForm.value.tagged_people1, this.instagramStoryForm.value.tagged_people2, this.instagramStoryForm.value.tagged_people3];
    const tagged_people_list2 = this.instagramStoryForm.value.tagged_people_list;
    const tagged_people = JSON.stringify(tagged_people_list1.concat(tagged_people_list2));

    this.caption =
      '@' + this.instagramStoryForm.value.tagged_people1 + ' ' +
      '@' + this.instagramStoryForm.value.tagged_people2 + ' ' +
      '@' + this.instagramStoryForm.value.tagged_people3 + ' ' ;
    return tagged_people;
  }

  taggedHashtag() {
    const tagged_hashtag_list1 = [this.instagramStoryForm.value.tagged_hashtag1, this.instagramStoryForm.value.tagged_hashtag2, this.instagramStoryForm.value.tagged_hashtag3];
    const tagged_hashtag_list2 = this.instagramStoryForm.value.tagged_hashtag_list;
    const tagged_hashtag = JSON.stringify(tagged_hashtag_list1.concat(tagged_hashtag_list2));

    this.caption = this.caption +
      '#' + this.instagramStoryForm.value.tagged_hashtag1 + ' ' +
      '#' + this.instagramStoryForm.value.tagged_hashtag2 + ' ' +
      '#' + this.instagramStoryForm.value.tagged_hashtag3
    return tagged_hashtag;
  }

  preview(files, event) {
    if (files.length === 0)
      return;
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }
  }

  get post_location() { return this.instagramStoryForm.get("post_location") };
  get recurrent_post_frequency() { return this.instagramStoryForm.get("recurrent_post_frequency") };
  get tagged_people1() { return this.instagramStoryForm.get("tagged_people1") };
  get tagged_people2() { return this.instagramStoryForm.get("tagged_people2") };
  get tagged_people3() { return this.instagramStoryForm.get("tagged_people3") };
  get tagged_hashtag1() { return this.instagramStoryForm.get("tagged_hashtag1") };
  get tagged_hahstag2() { return this.instagramStoryForm.get("tagged_hahstag2") };
  get tagged_hashtag3() { return this.instagramStoryForm.get("tagged_hahstag3") };
  get post_at() { return this.instagramStoryForm.get("post_at") };
  get img() { return this.instagramStoryForm.get("img") };

}
