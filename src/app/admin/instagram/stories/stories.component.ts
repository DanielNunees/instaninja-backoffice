import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { PostInstagramService } from 'src/app/_services/post-instagram.service';
import { InstagramStory } from 'src/app/_models/instagram-story';

@Component({
  selector: 'app-stories',
  templateUrl: './stories.component.html',
  styleUrls: ['./stories.component.scss']
})
export class StoriesComponent implements OnInit {

  loaded: boolean = false;
  subscription: Subscription = new Subscription();
  instagramStory_posted: InstagramStory[];
  instagramStory_sheduled: InstagramStory[];
  constructor(private postInstagramService: PostInstagramService) { }

  ngOnInit() {
    this.getAllPosts();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  getAllPosts() {
    const subs = this.postInstagramService.getAllStories().subscribe(data => {
      this.instagramStory_posted = data[1];
      this.instagramStory_sheduled = data[0];
      this.loaded = true;
    });
    this.subscription.add(subs);
  }

}
