import { NgModule } from '@angular/core';
import { PostComponent } from './post/post.component';
import { StoriesComponent } from './stories/stories.component';
import { InstagramRoutingModule } from './instagram-routing.module';
import {  NbAccordionModule, NbSelectModule, NbDatepickerModule, NbTabsetModule, NbDialogModule, NbInputModule } from '@nebular/theme';
import { NewPostFormComponent } from './post/new-post-form/new-post-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { SharedModule } from 'src/app/shared.module';
import { NewStoryFormComponent } from './stories/new-story-form/new-story-form.component';



@NgModule({
    declarations: [
        PostComponent,
        StoriesComponent,
        NewPostFormComponent,
        NewStoryFormComponent,

    ],
    imports: [
        NbSelectModule,
        NbDatepickerModule.forRoot(),
        NbDialogModule.forChild(),
        NbTabsetModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        NbAccordionModule,
        ReactiveFormsModule,
        InstagramRoutingModule,
        SharedModule
    ]
})
export class InstagramModule { }
