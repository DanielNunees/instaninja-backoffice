import { Component, OnInit } from '@angular/core';
import { PostInstagramService } from 'src/app/_services/post-instagram.service';
import { InstagramPost } from 'src/app/_models/instagram-post';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {
  loaded: boolean = false;
  subscription: Subscription = new Subscription();
  instagramPosts_posted: InstagramPost[];
  instagramPosts_sheduled: InstagramPost[];
  constructor(private postInstagramService: PostInstagramService) { }

  ngOnInit() {
    this.getAllPosts();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  getAllPosts() {
    const subs = this.postInstagramService.getAllPosts().subscribe(data => {
      this.instagramPosts_posted = data[1];
      this.instagramPosts_sheduled = data[0];
      this.loaded = true;
    });
    this.subscription.add(subs);
  }

}
