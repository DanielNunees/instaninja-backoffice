import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PostInstagramService } from 'src/app/_services/post-instagram.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { NbDialogService } from '@nebular/theme';
import { FindLocationComponent } from 'src/app/_components/dialog/find-location/find-location.component';
@Component({
  selector: 'app-new-post-form',
  templateUrl: './new-post-form.component.html',
  styleUrls: ['./new-post-form.component.scss']
})
export class NewPostFormComponent implements OnInit {
  instagramPostForm: FormGroup;
  submitted: boolean;
  imagePath: any;
  options: any;
  imgURL: any;
  start_date: Date = new Date();
  start_date_string: string = new Date().toDateString();
  post_id: string;
  location: any
  subscription: Subscription = new Subscription();
  public message: string;
  constructor(private fb: FormBuilder,
    private postInstagramService: PostInstagramService,
    private route: ActivatedRoute,
    private router: Router, private dialogService: NbDialogService) { }


  /**
   * It's just possible, for now, to tag 3 friends at same time
   * Just one location
   * One image
   * Obs. Album post coming soon.
   */

  ngOnInit() {
    this.instagramPostForm = this.fb.group({
      post_location: ['', { disabled: true }],
      comment_fav: [''],
      comment: [''],
      tagged_people_list: [''],
      recurrent_post_frequency: [''],
      tagged_people1: [''],
      tagged_people2: [''],
      tagged_people3: [''],
      caption: ['', Validators.required],
      post_at: ['', Validators.required],
      img: ['', Validators.required],

    });
    this.getAllOptions();
    this.getPost();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  open() {
    const hasScroll = true;
    this.dialogService.open(FindLocationComponent, { hasScroll })
      .onClose.subscribe(location => { this.location = location });
  }

  /**
   * This create a new Favorite comment
   * its showed the last three saved.
   */
  createNewComment() {
    if (this.comment.value && this.comment.value != "") {
      let comment = { "comment": this.comment.value };
      const subs = this.postInstagramService.createNewComment(comment).subscribe(data => {
      });
      this.subscription.add(subs);
    }
  }

  /**
   * Get one post from the database to edit, and show the data in the same form(New post form)
   * 
   */
  getPost() {
    this.post_id = this.route.snapshot.paramMap.get('id');
    if (this.post_id) {
      const subs = this.postInstagramService.getPost(this.post_id).subscribe(data => {
        this.instagramPostForm.patchValue(data[0]);
        this.start_date = new Date(data[0].post_at);
        this.start_date_string = this.start_date.toLocaleString();

        this.imgURL = data[0].post_media[0].url;
        this.instagramPostForm.patchValue({ img: '' });

        //Get people tagged
        var i = 0;
        let aux: Array<any> = [];
        data[0].post_user_tags.forEach(element => {
          aux[i] = element.username;
          i++;
        });
        this.instagramPostForm.patchValue({ tagged_people1: aux[0] });
        this.instagramPostForm.patchValue({ tagged_people2: aux[1] });
        this.instagramPostForm.patchValue({ tagged_people3: aux[2] });

      });
      this.subscription.add(subs);
    }

  }


  getAllOptions() {
    const subs = this.postInstagramService.getAllOptions().subscribe(data => {
      this.options = data;
    });
    this.subscription.add(subs);
  }

  onSubmit() {
    this.submitted = true;
    if (!this.instagramPostForm.valid)
      return;

    let file: File = this.imagePath[0];
    let formData: FormData = new FormData();

    const comment = this.instagramPostForm.value.comment_fav + " " + this.instagramPostForm.value.comment;
    const tagged_people_list1 = [this.instagramPostForm.value.tagged_people1, this.instagramPostForm.value.tagged_people2, this.instagramPostForm.value.tagged_people3];
    const tagged_people_list2 = this.instagramPostForm.value.tagged_people_list;
    const tagged_people = JSON.stringify(tagged_people_list1.concat(tagged_people_list2));
    let post_location = null;
    if (this.location) {
      post_location = JSON.stringify(this.location.location);
    }

    var tzoffset = (new Date(this.instagramPostForm.value.post_at)).getTimezoneOffset() * 60000; //offset in milliseconds
    var post_at = (new Date(this.instagramPostForm.value.post_at - tzoffset)).toISOString().slice(0, -1);

    formData.append('id', this.post_id);
    formData.append('photo', file, file.name);
    formData.append('post_location', post_location);
    formData.append('comment', comment);
    formData.append('recurrent_post_frequency', this.instagramPostForm.value.recurrent_post_frequency);
    formData.append('tagged_people', tagged_people);
    formData.append('caption', this.instagramPostForm.value.caption);
    formData.append('post_at', post_at.toString());

    this.instagramPostForm.value.aux = formData;

    this.postInstagramService.createNewPost(formData).subscribe(data => {
      //this.router.navigate(['/admin/instagram/posts']);
    });
  }



  preview(files, event) {
    if (files.length === 0)
      return;
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }
  }

  get post_location() { return this.instagramPostForm.get("post_location") }
  get comment_fav() { return this.instagramPostForm.get("comment_fav") }
  get comment() { return this.instagramPostForm.get("comment") }
  get recurrent_post_frequency() { return this.instagramPostForm.get("recurrent_post_frequency") }
  get tagged_people1() { return this.instagramPostForm.get("tagged_people1") }
  get tagged_people2() { return this.instagramPostForm.get("tagged_people2") }
  get tagged_people3() { return this.instagramPostForm.get("tagged_people3") }
  get caption() { return this.instagramPostForm.get("caption") }
  get post_at() { return this.instagramPostForm.get("post_at") }
  get img() { return this.instagramPostForm.get("img") }
}
