import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostComponent } from './post/post.component';
import { AuthGuard } from 'src/app/auth/auth.guard';
import { StoriesComponent } from './stories/stories.component';
import { NewPostFormComponent } from './post/new-post-form/new-post-form.component';
import { NewStoryFormComponent } from './stories/new-story-form/new-story-form.component';
const instagramRoutes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        children: [
            {
                path: 'posts',
                component: PostComponent,
                canActivate: [AuthGuard],
            },
            {
                path: 'stories',
                component: StoriesComponent,
                canActivate: [AuthGuard],
            },
            {
                path: 'new-story',
                component: NewStoryFormComponent,
                canActivate: [AuthGuard],
            },
            {
                path: 'new-post',
                component: NewPostFormComponent,
                canActivate: [AuthGuard],
            },
            {
                path: 'edit-post/:id',
                component: NewPostFormComponent,
                canActivate: [AuthGuard],
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(instagramRoutes)],
    exports: [RouterModule],
    providers: []
})
export class InstagramRoutingModule { }
