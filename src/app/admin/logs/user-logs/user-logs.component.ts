import { Component, OnInit } from '@angular/core';
import { UserLog } from 'src/app/_models/user-logs';
import { UserStatsService } from 'src/app/_services/user-stats.service';
import { LogsService } from 'src/app/_services/logs.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user-logs',
  templateUrl: './user-logs.component.html',
  styleUrls: ['./user-logs.component.scss']
})
export class UserLogsComponent implements OnInit {

  userLogs: UserLog[];
  loading = true;
  page = 1;
  pageSize = 10;
  subscription: Subscription = new Subscription();
  collectionSize = 10;
  constructor(private userStatsService: LogsService) { }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
  ngOnInit() {
    this.getUserLogs();
  }


  getUserLogs() {
    this.collectionSize = 100;
    const sub = this.userStatsService.getUserLogs().subscribe(data => {
      this.userLogs = data;
      this.loading = false;
    });
    this.subscription.add(sub);
  }

  get logs(): UserLog[] {
    this.collectionSize = this.userLogs.length < 100 ? this.userLogs.length : 100;

    return this.userLogs.map((country, i) => ({ id: i + 1, ...country }))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

}
