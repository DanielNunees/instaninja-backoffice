import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/auth/auth.guard';
import { UserLogsComponent } from './user-logs/user-logs.component';
import { SystemLogsComponent } from './system-logs/system-logs.component';
import { ExceptionLogsComponent } from './exception-logs/exception-logs.component';

const logsRoutes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        children: [
            {
                path: 'user-logs',
                component: UserLogsComponent,
                canActivate: [AuthGuard],
            },
            {
                path: 'system-logs',
                component: SystemLogsComponent,
                canActivate: [AuthGuard],
            },
            {
                path: 'exception-logs',
                component: ExceptionLogsComponent,
                canActivate: [AuthGuard],
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(logsRoutes)],
    exports: [RouterModule],
    providers: []
})
export class LogsRoutingModule { }
