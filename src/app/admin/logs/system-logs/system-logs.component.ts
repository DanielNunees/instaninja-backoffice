import { Component, OnInit } from '@angular/core';
import { LogsService } from 'src/app/_services/logs.service';
import { SystemLog } from 'src/app/_models/system-logs';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-system-logs',
  templateUrl: './system-logs.component.html',
  styleUrls: ['./system-logs.component.scss']
})
export class SystemLogsComponent implements OnInit {
  logs: SystemLog[];
  subscription: Subscription = new Subscription();
  loading:boolean = true;

  constructor(private logsService: LogsService) { }

  ngOnInit() {
    this.getAllSystemLogs();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  getAllSystemLogs() {
    const sub = this.logsService.getSystemLogs().subscribe(data => {
      this.logs = data;
      this.loading = false;
    });
    this.subscription.add(sub);
  }

}
