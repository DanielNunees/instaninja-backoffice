import { Component, OnInit } from '@angular/core';
import { ExceptiongLog } from 'src/app/_models/exception-logs';
import { LogsService } from 'src/app/_services/logs.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-exception-logs',
  templateUrl: './exception-logs.component.html',
  styleUrls: ['./exception-logs.component.scss']
})
export class ExceptionLogsComponent implements OnInit {

  logs: ExceptiongLog[];
  subscription: Subscription = new Subscription();
  loading: boolean = true;

  constructor(private logsService: LogsService) { }


  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
  ngOnInit() {
    this.getAllExceptionLogs();
  }

  getAllExceptionLogs() {
    const sub = this.logsService.getExceptionLogs().subscribe(data => {
      this.logs = data;
      this.loading = false;
    });
    this.subscription.add(sub);
  }

}
