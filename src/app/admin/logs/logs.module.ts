import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserLogsComponent } from './user-logs/user-logs.component';
import { SystemLogsComponent } from './system-logs/system-logs.component';
import { ExceptionLogsComponent } from './exception-logs/exception-logs.component';
import { LogsRoutingModule } from './logs-routing.module';
import { NbCardModule } from '@nebular/theme';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';



@NgModule({
    declarations: [
        UserLogsComponent,
        SystemLogsComponent,
        ExceptionLogsComponent,
    ],
    imports: [
        CommonModule,
        LogsRoutingModule,
        NbCardModule,
        NgbPaginationModule,
        FormsModule
    ]
})
export class LogsModule { }
