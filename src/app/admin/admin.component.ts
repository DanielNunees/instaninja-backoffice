import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { Router } from '@angular/router';
import { NbAuthService, NbAuthJWTToken } from '@nebular/auth';
import { NbSidebarService } from '@nebular/theme';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
  styles: [`
  :host nb-layout-header button:last-child {
    margin-left: auto;
  }
  :host nb-layout-header nb-user{
    actions-text-color: white;
  }
`],

})
export class AdminComponent implements OnInit {
  user: any = {};
  constructor(private authenticationService: NbAuthService,
    private router: Router,
    private authService: NbAuthService,
    private sidebarService: NbSidebarService) {
    this.authService.onTokenChange()
      .subscribe((token: NbAuthJWTToken) => {

        if (token.isValid()) {
          this.user = token.getPayload(); // here we receive a payload from the token and assigns it to our `user` variable 
        }

      });
  }

  ngOnInit() {
  }

  toggle() {
    this.sidebarService.toggle(true, 'left');
  }

  items = [
    {
      title: 'Dashboard',
      icon: 'person-outline',
      link: ['/'],
    },
    {
      title: 'User Settigns',
      icon: 'lock-outline',
      children: [
        {
          title: 'People',
          link: ['/admin/user-settings/people'],
        },
        {
          title: 'References',
          link: ['/admin/user-settings/references'],
        }
      ]
    },
    {
      title: 'Instagram',
      icon: 'person-outline',
      children: [
        {
          title: 'Posts',
          link: ['/admin/instagram/posts'],
        },
        {
          title: 'Stories',
          link: ['/admin/instagram/stories'],
        }
      ]
    },
    {
      title: 'Logs',
      icon: 'checkmark-outline',
      children: [
        {
          title: 'User Logs',
          link: ['/admin/logs/user-logs'],
        },
        {
          title: 'System Logs',
          link: ['/admin/logs/system-logs'],
        },
        {
          title: 'Exception Logs',
          link: ['/admin/logs/exception-logs'],
        }]
    },
  ];

  logout() {
    this.authenticationService.logout('email').subscribe(data => {
      this.router.navigate(['/auth/login']);

    });

  }
}
