import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthService } from '../auth/auth.service';
import { tap } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { NbToastrService } from '@nebular/theme';
import { NbTokenService } from '@nebular/auth';

@Injectable({
    providedIn: 'root'
})
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: NbTokenService,
        private loaderService: NgxSpinnerService,
        private toastrService: NbToastrService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        window.scroll(0, 0);
        this.showLoader();
        this.authenticationService.get().subscribe(token => {
            if (token) {
                request = request.clone({
                    setHeaders: {
                        Authorization: `Bearer ${token}`
                    }
                });
            }
        });



        return next.handle(request).pipe(tap((
            event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
                this.onEnd();
                if (event.body.success)
                    this.showToast("top-end", event.body.success, "Success", 'success');
                if (event.body.info)
                    this.showToast("top-end", event.body.info, "Info", 'info');
                if (event.body.error)
                    this.showToast("top-end", event.body.error, "Error", 'danger');
            }
        },
            (err: any) => {
                this.onEnd();
            }));
    }

    private onEnd(): void {
        this.hideLoader();

    }

    showToast(position, message, title, status) {
        const duration = 0;
        this.toastrService.show(
            message,
            title,
            { position, status, duration });
    }
    private showLoader(): void {
        this.loaderService.show();
    }
    private hideLoader(): void {
        this.loaderService.hide();
    }
}
