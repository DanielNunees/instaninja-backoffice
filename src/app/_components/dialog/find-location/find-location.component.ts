import { Component } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { UserSettingsService } from 'src/app/_services/user-settings.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-find-location',
  templateUrl: './find-location.component.html',
  styleUrls: ['./find-location.component.scss']
})
export class FindLocationComponent {
  subscription: Subscription = new Subscription();
  places: any;
  constructor(protected ref: NbDialogRef<FindLocationComponent>, private userSettingsService: UserSettingsService) { }

  cancel() {
    this.ref.close();
  }

  submit(name) {
    this.ref.close(name);
  }

  search(value) {
    const subs = this.userSettingsService.findPlaces(value).subscribe(data => {
      this.places = data;
    });
    this.subscription.add(subs);
  }
}
