import { Component, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
    selector: 'app-dialog',
    templateUrl: './verification-dialog.component.html',
})
export class VerificationDialogComponent implements OnInit {

    constructor(protected ref: NbDialogRef<VerificationDialogComponent>) { }

    ngOnInit() {
    }

    cancel() {
        this.ref.close();
    }

    submit(name) {
        this.ref.close(name);
    }
}
