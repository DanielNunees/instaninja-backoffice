export class UserStats {

    constructor(id: number,
        user_id: number,
        followers_count: number,
        following_count: number,
        media_count: number,
        total_followed: number,
        total_following_back: number,
        total_user_media_likes: number,
        total_hashtag_media_likes: number,
        total_location_media_likes: number,
        created_at: Date,
        updated_at: Date) {

    }
}