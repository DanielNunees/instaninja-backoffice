export class UserLog {
    user_id: string;
    username: string;
    code: string;
    message: string;
    created_at: Date;
}
