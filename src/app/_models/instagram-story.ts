export class InstagramStory {
    id: number;
    user_id: number;
    location_id: number;
    comment_id: number;
    recurrent_post: number;
    frequency: string;
    tagged_people_pattern: string;
    caption: string;
    post_at: Date;
    img: FormData;
};
