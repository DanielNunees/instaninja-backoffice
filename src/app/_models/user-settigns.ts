export class UserSettigns {
    user_id: number;
    stop_follow_request: number;
    throttled_request_time: number;
    max_following: number;
    follow_max: number;
    follow_min: number;
    follow_time_max: number;
    follow_time_min: number;
    unfollow_max: number;
    unfollow_min: number;
    unfollow_time_max: number;
    unfollow_time_min: number;
    like_max: number;
    like_min: number;
    like_time_max: number;
    like_time_min: number;
    daily_posts: number;
    daily_stories: number;
    schedule_timeline_post: number;
    schedule_story_post
}