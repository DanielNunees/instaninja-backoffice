export class SystemLog {
    id: number;
    user_id: string;
    username: string;
    code: string;
    message: string;
    message_type: string;
    created_at: Date;
    updated_at: Date;
}
