export class ExceptiongLog {
    id: number;
    user_id: string;
    username: string;
    exception: string;
    exception_type: string;
    code: number;
    created_at: Date;
    updated_at: Date;
}
