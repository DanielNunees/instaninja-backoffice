export class ReferenceProfile {

    constructor(id: number,
        user_id: number,
        people_id: number,
        total_likes: number,
        username: number,
        total_follow: number,
        follow_back: number,
        followers_max_id: number,
        reference_profile_user_id: number,
        is_active: number,
        created_at: Date,
        updated_at: Date) {

    }
}