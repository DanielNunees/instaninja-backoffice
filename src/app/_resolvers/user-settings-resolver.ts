import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UserSettingsService } from '../_services/user-settings.service';

@Injectable()
export class UserSettingsResolver implements Resolve<any> {
  constructor(private userSettingService: UserSettingsService) { }

  resolve() {
    return this.userSettingService.getAllReferenceProfiles();
  }
}